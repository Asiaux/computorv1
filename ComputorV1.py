import re
simp = re.compile('((?P<egal1>[=]?)\s*(?P<sign1>[+-]?)\s*?(?P<multip>[0-9.]*|[0-9]*)\s*(?P<X>[xX])(?P<powa>[0-9]?))|((?P<egal2>[=]?)\s*(?P<sign2>[+\-]?)\s*?(?P<nbs>[0-9.]+))')
comp = re.compile('(?P<egal>[=]?)\s*(?P<sign>[+-])?\s*(?P<multip>[0-9.]+)\s*\*\s*[xX]\^(?P<powa>\d+)')


def calcul_simp_pattern(pat, inp):
	res = [0, 0, 0]  # res[0] = nombres sans X, res[1] = multiple de X, res[2] = multiple X^2
	egal = False
	while pat:
		if pat.group('egal1') or pat.group('egal2'):
			egal = True
		if pat.group('X') is not None:
			if not egal and (not pat.group('sign1') or pat.group('sign1') == '+'):
				res[int(pat.group('powa')) if pat.group('powa') else 1] += (float(pat.group('multip')) if pat.group('multip') else 1)
			else:
				res[int(pat.group('powa')) if pat.group('powa') else 1] -= (float(pat.group('multip')) if pat.group('multip') else 1) * -1 if pat.group('sign1') == '-' else 1
		else:
			if not egal and (not pat.group('sign2') or pat.group('sign2' == '+')):
				res[0] += float(pat.group('nbs'))
			else:
				res[0] -= float(pat.group('nbs')) * -1 if pat.group('sign1') == '-' else 1
		inp = simp.sub('', inp, 1)
		pat = simp.search(inp)
	if res[0] != 0:
		print("Le degré polynomial est 2")
	elif res[1] != 0:
		print("Le degré polynomial est 1")
	else:
		print("Le degré polynomial est 0")
	return res


def calcul_comp_pattern(pat, inp):
	res = [0, 0, 0]  # res[0] = nombres X^0, res[1] = multiple de X^1, res[2] = multiple X^2
	egal = False
	while pat:
		print(pat.group('multip'))
		if pat.group('egal') is not None:
			egal = True
		if not egal and (not pat.group('sign') or pat.group('powa') == '+'):
			res[int(pat.group('powa'))] += float(pat.group('multip'))
		else:
			res[int(pat.group('powa'))] -= float(pat.group('multip')) * -1 if pat.group('sign1') == '-' else 1
		inp = comp.sub('', inp, 1)
		pat = comp.search(inp)
	if res[0] != 0:
		print("Le degré polynomial est 2")
	elif res[1] != 0:
		print("Le degré polynomial est 1")
	else:
		print("Le degré polynomial est 0")
	return res


def simplification(dividend, diviseur):
	if diviseur < 0 and dividend < 0:
		diviseur, dividend = -diviseur, -dividend
	if dividend.is_integer() and diviseur.is_integer():
		if (dividend / diviseur).is_integer():
			return dividend / diviseur
		else:
			x = dividend
			while x > 1:
				if (dividend/x).is_integer() and (diviseur/x).is_integer():
					dividend, diviseur, x = dividend/x, diviseur/x, dividend
				x -= 1
			return '{}/{}'.format(int(dividend), int(diviseur))
	else:
		return '{}/{}'.format(dividend, diviseur)


def solution(disc, mult): #mult[0] = nombres sans X, mult[1] = multiple de X, mult[2] = multiple X^2
	if mult[1] and mult[2]:
		print('Le discriminant est :', disc)
	if disc == 0 and mult[2] != 0 and mult[1] != 0:
		print('Il existe une solution qui est : {}'.format((-(mult[1]/(2 * mult[2])))))
	elif disc > 0 and mult[2] != 0 and mult[1] != 0:
		print('Il existe deux solutions qui sont : {} and {}'.format((-mult[1] - disc ** 0.5) / (2 * mult[2]), (-mult[1] + disc ** 0.5) / (2 * mult[2])))
	elif mult[2] == 0 and mult[1] != 0 and mult[1] != 1 and mult[0] != 0:
		print('The solution is : x =', simplification(-mult[0], mult[1]))
	elif mult[2] == 0 and mult[1] == 1 and mult[0] != 0:
		print('The solution is : x =', -mult[0])
	elif mult[2] != 0 and mult[2] !=1 and mult[1] == 0 and mult[0] != 0:
		print('The solution is : x =\u221A{}'.format(simplification(-mult[0], mult[2])))
	elif mult[2] == 1 and mult[1] == 0 and mult[0] != 0:
		print('The solution is : x =\u221A{}'.format(-mult[0]))
	else:
		print('Il n\'existe aucune solution a cette equation')


if __name__ == '__main__':
	while 1:
		inp = input("equation du 2nd degre plz : ")
		wrong_pattern = re.search('([a-wyzA-WYZ]|[\d]*\.[\d]+\.[\d]+|X\^[0-9]\.[0-9]+|X\^[0-9]{2,}|X\^[3-9]|X\^-[0-9])', inp)  # find letters, bad numbers(1.1.1...), bad powaaa X^3+ X^-1
		if wrong_pattern:
			print("Mauvaise entrée, essayez avec la forme 1 + X^0 2 + X^1 3 + X^2 = 0 X^0 ...., sans lettres autres que xX ou nombres comme 9.9.9.9")
		else:
			break
	simp_pattern = simp.search(inp)
	comp_pattern = comp.search(inp)
	if simp_pattern:
		mult = calcul_simp_pattern(simp_pattern, inp)
		inp = re.sub(simp, '', inp, 1)
	else:
		mult = calcul_comp_pattern(comp_pattern, inp)
	print('La forme simplifié est : {}{}{} = 0'
	.format('{}X² '.format(mult[2]) if int(mult[2]) >= 0 else '- {}X² '.format(abs(mult[2])),
	'+ {}X '.format(mult[1]) if int(mult[1]) >= 0 else '- {}X '.format(abs(mult[1])),
	'+ {}'.format(mult[0]) if int(mult[0]) >= 0 else '- {}'.format(abs(mult[0]))))
	solution(mult[1] * mult[1] - (mult[0] * mult[2] * 4), mult)

# Examples:
#  5 * X^0 + 4 * X^1 - 9.3 * X^2 = 1 * X^0
#  4 * X^0 + 4 * X^1 - 9.3 * X^2 = 0
#  5 * X^0 + 4 * X^1 = 4 * X^0
#  1 * X^0 + 4 * X^1 = 0
#  8 * X^0 - 6 * X^1 + 0 * X^2 - 5.6 * X^3 = 3 * X^0
#  5 * X^0 - 6 * X^1 + 0 * X^2 - 5.6 * X^3 = 0
#  5 + 4 * X^0 + X^2= X^2
#  0*X^2-5*X^1-10*X^0=0
#  X2-5X-10=0
# 9.3X + 18.65 + 22.542X2 + 1.1X = -15465.165413X + 5
# 0.0012067496547532425 et 685.597471850292